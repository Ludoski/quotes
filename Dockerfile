# First stage: complete build environment
FROM maven:3.9.3-eclipse-temurin-20 AS builder

# add pom.xml and source code
ADD ./pom.xml pom.xml
ADD ./src src/

# package jar
RUN mvn clean package

# Second stage: minimal runtime environment
FROM eclipse-temurin:20-jdk-alpine

# copy jar from the first stage
COPY --from=builder target/quotes-0.0.1-SNAPSHOT.jar application.jar

EXPOSE 2551

CMD ["java", "-jar", "application.jar"]