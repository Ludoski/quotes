package com.company.quotes.exceptions.auth;

import com.company.quotes.exceptions.ApplicationException;
import org.springframework.http.HttpStatus;

/**
 * Bad token and invalid user authorization exception
 */
public class BadTokenException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "You provided bad token and are not authorized to access this resource.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.UNAUTHORIZED;

  public BadTokenException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
