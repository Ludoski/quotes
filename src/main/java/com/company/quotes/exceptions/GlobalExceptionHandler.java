package com.company.quotes.exceptions;

import com.company.quotes.dtos.response.ResponseBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.Map;

/**
 * Global exception handler that packs custom response for client according to exception
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

  /**
   * Handle custom application exceptions
   * @param exception     Custom application exception
   * @return              Response message
   */
  @ExceptionHandler(ApplicationException.class)
  public ResponseEntity<Map<String, Object>> handle(ApplicationException exception) {
    HttpStatus status = exception.getHttpStatus();
    String message = exception.getResponseMessage();
    return createResponse(status, message);
  }

  /**
   * Handle MissingServletRequestParameterException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(MissingServletRequestParameterException.class)
  public ResponseEntity<Map<String, Object>> handle(MissingServletRequestParameterException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle HttpMediaTypeNotSupportedException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public ResponseEntity<Map<String, Object>> handle(HttpMediaTypeNotSupportedException exception) {
    HttpStatus status = HttpStatus.UNSUPPORTED_MEDIA_TYPE;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle MethodArgumentTypeMismatchException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public ResponseEntity<Map<String, Object>> handle(MethodArgumentTypeMismatchException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle HttpRequestMethodNotSupportedException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<Map<String, Object>> handle(HttpRequestMethodNotSupportedException exception) {
    HttpStatus status = HttpStatus.METHOD_NOT_ALLOWED;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle HttpMessageNotReadableException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<Map<String, Object>> handle(HttpMessageNotReadableException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle MethodArgumentNotValidException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, Object>> handle(MethodArgumentNotValidException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    StringBuilder message = new StringBuilder();
    for (ObjectError error: exception.getBindingResult().getAllErrors()) {
      message.append(((FieldError) error).getField()).append(" ").append(error.getDefaultMessage()).append(" ");
    }
    return createResponse(status, message.toString().trim());
  }

  /**
   * Handle IllegalArgumentException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<Map<String, Object>> handle(IllegalArgumentException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle all the other exceptions
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, Object>> handle(Exception exception) {
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    String message = exception.getClass().toString();
    return createResponse(status, message);
  }

  /**
   * Method for packing custom http response
   * @param status      Http response status
   * @param message     Http response message
   * @return            Response message
   */
  private ResponseEntity<Map<String, Object>> createResponse(HttpStatus status, String message) {
    Map<String, Object> body = new ResponseBody(status, message).toMap();
    return new ResponseEntity<>(body, status);
  }

}
