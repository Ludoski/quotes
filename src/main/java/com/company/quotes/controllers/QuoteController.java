package com.company.quotes.controllers;

import com.company.quotes.dtos.quote.QuoteDTO;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;

/**
 * Quotes controller
 */
public interface QuoteController {

  /**
   * Get quotes depending on your user role
   * @param optionalToken     Optional jwt token
   * @return                  Quote depends on user role
   */
  @GetMapping(path = "/quote", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  QuoteDTO getQuote(@RequestHeader("Authorization") Optional<String> optionalToken);

}
