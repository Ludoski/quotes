package com.company.quotes.controllers.impl;

import com.company.quotes.controllers.QuoteController;
import com.company.quotes.dtos.quote.QuoteDTO;
import com.company.quotes.services.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * @see QuoteController
 */
@RestController
public class QuoteControllerImpl implements QuoteController {

  @Autowired
  private QuoteService quoteService;

  /**
   * @see QuoteController#getQuote(Optional)
   */
  @Override
  public QuoteDTO getQuote(Optional<String> optionalToken) {
    return quoteService.getQuote(optionalToken);
  }

}
