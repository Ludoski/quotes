package com.company.quotes.dtos.quote;

import com.company.quotes.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 * Quote DTO response
 */
@Getter
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuoteDTO {

    @JsonProperty("user_role")
    private UserRole userRole;

    @JsonProperty
    private String quote;

}
