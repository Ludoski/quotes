package com.company.quotes.enums;

/**
 * User roles
 */
public enum UserRole {

  UNAUTHORIZED,
  ROLE_USER,
  ROLE_ADMIN,
  UNSUPPORTED

}
