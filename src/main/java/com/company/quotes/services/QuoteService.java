package com.company.quotes.services;

import com.company.quotes.dtos.quote.QuoteDTO;

import java.util.Optional;

/**
 * Quotes service
 */
public interface QuoteService {

  /**
   * Get quotes depending on your user role
   * @param optionalToken     Optional jwt token
   * @return                  Quote depends on user role
   */
  QuoteDTO getQuote(Optional<String> optionalToken);

}
