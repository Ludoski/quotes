package com.company.quotes.services;

import com.company.quotes.enums.UserRole;

/**
 * Jwt token helper service
 */
public interface JwtService {

  /**
   * Checks if user has role
   * @param token       User jwt token
   * @param role        User role to check
   * @return            True if user has role, false if not
   */
  boolean hasRole(String token, UserRole role);

}
