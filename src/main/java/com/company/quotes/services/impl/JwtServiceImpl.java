package com.company.quotes.services.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.company.quotes.enums.UserRole;
import com.company.quotes.exceptions.auth.BadTokenException;
import com.company.quotes.services.JwtService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @see JwtService
 */
@Service
public class JwtServiceImpl implements JwtService {

  @Value("${spring.security.jwt.secret}")
  private String jwtSecretKey;

  /**
   * @see JwtService#hasRole(String, UserRole)
   */
  @Override
  public boolean hasRole(String token, UserRole role) {
    // Remove "Bearer " from token
    String trimmedToken = StringUtils.removeStart(token, "Bearer").trim();

    try {
      // Verify token
      Algorithm algorithm = Algorithm.HMAC256(jwtSecretKey);
      JWTVerifier verifier = JWT.require(algorithm)
              .build(); //Reusable verifier instance
      DecodedJWT jwt = verifier.verify(trimmedToken);

      // Get claims
      Map<String, Claim> claims = jwt.getClaims();

      return claims.get("roles").asString().contains(role.toString());
    } catch (TokenExpiredException | JWTDecodeException | IllegalArgumentException e) {
      // If exception is thrown
       throw new BadTokenException();
    }
  }

}
