package com.company.quotes.services.impl;

import com.company.quotes.dtos.quote.QuoteDTO;
import com.company.quotes.enums.UserRole;
import com.company.quotes.exceptions.auth.BadTokenException;
import com.company.quotes.services.JwtService;
import com.company.quotes.services.QuoteService;
import com.company.quotes.storage.Quotes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

/**
 * @see QuoteService
 */
@Service
public class QuoteServiceImpl implements QuoteService {

  @Autowired
  private JwtService jwtService;

  private static final Random random = new Random();

  /**
   * @see QuoteService#getQuote(Optional)
   */
  @Override
  public QuoteDTO getQuote(Optional<String> optionalToken) {

    // Return unauthorized quote if user is unauthorized and do not have any authorization token in request header
    if (optionalToken.isEmpty()) {
      return QuoteDTO.builder()
              .userRole(UserRole.UNAUTHORIZED)
              .quote(getRandomQuote(Quotes.unauthorizedQuotes))
              .build();
    }

    String token = optionalToken.get();

    try {
      // Return admin quote if user has admin role
      if (jwtService.hasRole(token, UserRole.ROLE_ADMIN)) {
        return QuoteDTO.builder()
                .userRole(UserRole.ROLE_ADMIN)
                .quote(getRandomQuote(Quotes.adminQuotes))
                .build();
      }

      // Return user quote if user has user role
      if (jwtService.hasRole(token, UserRole.ROLE_USER)) {
        return QuoteDTO.builder()
                .userRole(UserRole.ROLE_USER)
                .quote(getRandomQuote(Quotes.userQuotes))
                .build();
      }
    } catch (BadTokenException e) {
      // Return unauthorized quote if user is unauthorized and token is invalid
      return QuoteDTO.builder()
              .userRole(UserRole.UNAUTHORIZED)
              .quote(getRandomQuote(Quotes.unauthorizedQuotes))
              .build();
    }

    // Return message if user role is not supported
    return QuoteDTO.builder()
            .userRole(UserRole.UNSUPPORTED)
            .quote(getRandomQuote(Quotes.unsupportedQuotes))
            .build();
  }

  private String getRandomQuote(String[] quotes) {
    return quotes[random.nextInt(quotes.length)];
  }

}
