package com.company.quotes.storage;

/**
 * Quotes storage
 */
public class Quotes {

  private Quotes() {}

  public static final String[] unauthorizedQuotes = {
          "Securing a computer system has traditionally been a battle of wits: the penetrator tries to find the holes, and the designer tries to close them. - Gosser",
          "A computer lets you make more mistakes faster than any invention in human history - with the possible exceptions of handguns and tequila. - Mitch Ratliff",
          "It is much more secure to be feared than to be loved. - Niccolo Machiavelli",
          "Half of what you know about security will be obsolete in 18 months. - G. Mark Hardy (G. Mark's Law)",
          "You can't hold firewalls and intrusion detection systems accountable. You can only hold people accountable. - Daryl White, DOI CIO"
  };

  public static final String[] userQuotes = {
          "I do not fear computers. I fear lack of them. - Isaac Asimov",
          "A computer once beat me at chess, but it was no match for me at kick boxing. - Emo Philips",
          "Computer Science is no more about computers than astronomy is about telescopes. - Edsger W. Dijkstra",
          "The computer was born to solve problems that did not exist before. - Bill Gates",
          "Software is like entropy: It is difficult to grasp, weighs nothing, and obeys the Second Law of Thermodynamics; i.e., it always increases. - Norman Augustine"
  };

  public static final String[] adminQuotes = {
          "Rebooting is a wonder drug - it fixes almost everything. - Garrett Hazel",
          "Press any key to continue, or any other key to cancel. - Author Unknown",
          "If all else fails, read the documentation. - Author Unknown",
          "If it wasn't backed-up, then it wasn't important. - Author Unknown",
          "User is the word computer professionals use when they mean 'idiot'. - Dave Barry"
  };

  public static final String[] unsupportedQuotes = {
          "Against logic there is no armor like ignorance. - J. Peter",
          "When a thought is too weak to be expressed simply, it should be rejected. - Marquis de Vauvenargues",
          "Research is the process of going up alleys to see if they are blind. - Marston Bates",
          "I shall never be ashamed of citing a bad author if the line is good. - Seneca",
          "An effective way to deal with predators is to taste terrible. - Unknown"
  };

}
