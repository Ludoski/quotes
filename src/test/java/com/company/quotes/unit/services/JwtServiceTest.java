package com.company.quotes.unit.services;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.company.quotes.enums.UserRole;
import com.company.quotes.exceptions.auth.BadTokenException;
import com.company.quotes.services.impl.JwtServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class JwtServiceTest {

  private static final String mockedJwtSecretKey = "secret key";

  @InjectMocks
  private JwtServiceImpl jwtService;

  @BeforeEach
  void setup() {
    ReflectionTestUtils.setField(jwtService, "jwtSecretKey", mockedJwtSecretKey);
  }

  @Test
  void hasRoleUserTrueTest() {
     assertTrue(jwtService.hasRole(createJwtToken(), UserRole.ROLE_USER));
  }

  @Test
  void hasRoleAdminFalseTest() {
    assertFalse(jwtService.hasRole(createJwtToken(), UserRole.ROLE_ADMIN));
  }

  @Test
  void hasRoleInvalidTokenTest() {
    assertThatThrownBy(() -> jwtService.hasRole("invalid token", UserRole.ROLE_USER))
            .isInstanceOf(BadTokenException.class);
  }

  private String createJwtToken() {

    // Create expiration date
    Calendar expiresAt = Calendar.getInstance();
    expiresAt.add(Calendar.MINUTE, 60);

    // Create builder
    JWTCreator.Builder jwtBuilder = JWT.create().withSubject("username");

    UserRole[] roles = { UserRole.ROLE_USER };

    // Add claims
    Map<String, String> claims = new HashMap<>();
    claims.put("roles", Arrays.toString(roles));

    claims.forEach(jwtBuilder::withClaim);

    // Add expiredAt and etc
    return jwtBuilder
            .withNotBefore(Calendar.getInstance().getTime())
            .withExpiresAt(expiresAt.getTime())
            .sign(Algorithm.HMAC256(mockedJwtSecretKey));
  }

}
