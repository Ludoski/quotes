package com.company.quotes.unit.services;

import com.company.quotes.dtos.quote.QuoteDTO;
import com.company.quotes.enums.UserRole;
import com.company.quotes.exceptions.auth.BadTokenException;
import com.company.quotes.services.JwtService;
import com.company.quotes.services.impl.QuoteServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class QuoteServiceTest {

    @Mock
    private JwtService jwtService;

    @InjectMocks
    private QuoteServiceImpl quoteService;

    @Test
    @DisplayName("Return unauthorized quote")
    void getQuoteUnauthorizedTest() {
        Optional<String> optionalToken = Optional.empty();

        QuoteDTO result =  quoteService.getQuote(optionalToken);

        assertEquals(UserRole.UNAUTHORIZED, result.getUserRole());
        assertInstanceOf(String.class, result.getQuote());
    }

    @Test
    @DisplayName("Return admin quote")
    void getQuoteAdminTest() {
        Optional<String> optionalToken = Optional.of("admin token");

        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_ADMIN))
                .thenReturn(true);

        QuoteDTO result =  quoteService.getQuote(optionalToken);

        assertEquals(UserRole.ROLE_ADMIN, result.getUserRole());
        assertInstanceOf(String.class, result.getQuote());
    }

    @Test
    @DisplayName("Return user quote")
    void getQuoteUserTest() {
        Optional<String> optionalToken = Optional.of("user token");

        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_ADMIN))
                .thenReturn(false);
        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_USER))
                .thenReturn(true);

        QuoteDTO result =  quoteService.getQuote(optionalToken);

        assertEquals(UserRole.ROLE_USER, result.getUserRole());
        assertInstanceOf(String.class, result.getQuote());
    }

    @Test
    @DisplayName("Return unauthorized quote if token is invalid")
    void getQuoteTokenInvalidTest() {
        Optional<String> optionalToken = Optional.of("invalid token");

        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_ADMIN))
                .thenThrow(BadTokenException.class);

        QuoteDTO result =  quoteService.getQuote(optionalToken);

        assertEquals(UserRole.UNAUTHORIZED, result.getUserRole());
        assertInstanceOf(String.class, result.getQuote());
    }

    @Test
    @DisplayName("Return unsupported quote if user role is unknown")
    void getQuoteUnsupportedTest() {
        Optional<String> optionalToken = Optional.of("user token");

        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_ADMIN))
                .thenReturn(false);
        when(jwtService.hasRole(optionalToken.get(), UserRole.ROLE_USER))
                .thenReturn(false);

        QuoteDTO result =  quoteService.getQuote(optionalToken);

        assertEquals(UserRole.UNSUPPORTED, result.getUserRole());
        assertInstanceOf(String.class, result.getQuote());
    }

}
