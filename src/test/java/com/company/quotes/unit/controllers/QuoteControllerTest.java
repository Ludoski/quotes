package com.company.quotes.unit.controllers;

import com.company.quotes.controllers.impl.QuoteControllerImpl;
import com.company.quotes.services.QuoteService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class QuoteControllerTest {

    @Mock
    private QuoteService quoteService;

    @InjectMocks
    private QuoteControllerImpl quoteController;

    @Test
    @DisplayName("Successfully called service")
    void getQuoteTest() {
        quoteController.getQuote(any());

        verify(quoteService, times(1)).getQuote(any());
    }

}
